package com.example.mycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private lateinit var operationTextView: TextView
    private var operand = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)
        operationTextView = findViewById(R.id.operationTextView)
    }
    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            var number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }
            resultTextView.text = result + number
        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            operand = resultTextView.text.toString().toDouble()
            operation = clickedView.text.toString()
            resultTextView.text = "0"
            operationTextView.text = operation
        }
    }


    fun equalClick(clickedView: View) {
        val secOperand = resultTextView.text.toString().toDouble()
        when (operation) {
            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> resultTextView.text = (operand / secOperand).toString()
        }
        operation = ""
        operationTextView.text = operation
    }

    fun clearClick(clickedView: View) {
        if (clickedView is TextView) {
            operation = ""
            operationTextView.text = operation
            resultTextView.text = "0"
        }
    }

    fun delClick(clickedView: View) {
        if (clickedView is TextView) {
            resultTextView.text = resultTextView.text.toString().dropLast(1)

            if (resultTextView.text.toString() == ""){
                resultTextView.text = "0"
            }
        }
    }

    fun dotClick(clickedView: View) {
        if (clickedView is TextView) {
            if (!("." in resultTextView.text.toString())){
                resultTextView.text = resultTextView.text.toString() + "."
            }
        }
    }

}